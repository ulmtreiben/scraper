import * as moment from 'moment'
import * as ical from 'ical'
import { ScrapeFunction, ScrapedEvent } from '../index'
import Logger from '@trichter/logger'
const log = Logger('trichter-scraper/ical')

async function icalParseURL (url: string, options: any): Promise<any> {
  return new Promise((resolve, reject) => ical.fromURL(url, options, (err, data) => {
    if (err) reject(err)
    else resolve(data)
  }))
}
function decodeCharCodes(str) {
  return str.replace(/&#[0-9]+;/g, (a) => {
      return String.fromCharCode(parseInt(a.slice(2, a.length-1)))
  })
}

// TODO: use image attachments

const crawl: ScrapeFunction = async (options: {url: string}) => {
  if (!options || !options.url) throw new Error('option \'url\' must be provided')
  try {
    log.debug(`load ${options.url}`)
    const cal = await icalParseURL(options.url, {})

    return Object.keys(cal).map((id) => {
      const c = cal[id]
      if (!c.summary) return null
      const event: ScrapedEvent = {
        id: id.split('@')[0],
        title: c.summary,
        start: moment(c.start).toISOString(),
        end: c.end ? moment(c.end).toISOString() : null,
        address: c.location || null,
        link: typeof c.url === 'object' ? c.url.val : c.url,
        teaser: c.description ? decodeCharCodes(c.description).substr(0, 100) : null,
        description: c.description ? decodeCharCodes(c.description.replace(/\n/gm, '\n\n')) : null
      }
      return event
    })
      .filter((e) => {
        if (!e) return false
        if (moment(e.start).isBefore(moment())) return false
        return true
      })
  } catch (err) {
    log.error(err)
    return []
  }
}
export default crawl
